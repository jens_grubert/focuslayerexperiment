fe1TLX <- read.csv("TLX/tlx-1fe.csv", header=TRUE, sep=";")
fe2TLX <- read.csv("TLX/tlx-2fe.csv", header=TRUE, sep=";")
summary(fe1TLX); summary(fe2TLX)
##
# normality tests
# ar
shapiro.test(fe1TLX$md) # normal distributed
shapiro.test(fe1TLX$pd) # NOT normal distributed
shapiro.test(fe1TLX$td) # NOT normal distributed
shapiro.test(fe1TLX$p) # normal distributed
shapiro.test(fe1TLX$e) # normal distributed
shapiro.test(fe1TLX$Frustration) # NOT normal distributed
shapiro.test(fe1TLX$o) # normal distributed
#dm
shapiro.test(fe2TLX$md) # normal distributed
shapiro.test(fe2TLX$pd) # NOT normal distributed
shapiro.test(fe2TLX$td) # normal distributed
shapiro.test(fe2TLX$p) # normal distributed
shapiro.test(fe2TLX$e) # normal distributed
shapiro.test(fe2TLX$Frustration) # NOT normal distributed
shapiro.test(fe2TLX$o) # normal distributed


##
# compare 
ttestPairedBatt(fe1TLX$md, fe2TLX$md, "two.sided") # no sig diff.
wilcoxPairedBatt(fe1TLX$pd, fe2TLX$pd, "two.sided") # 
# V = 98, p-value = 0.03318
#Z = 2.1332, p-value = 0.03291
# alternative hypothesis: true mu is not equal to 0
# r 0.3658416 
# PD higher for HMD
summary(fe1TLX$pd) # (MD: 14,67)
sd(fe1TLX$pd) # 9.797167
summary(fe2TLX$pd) # 3.333
sd(fe2TLX$pd) # 8.00342

wilcoxPairedBatt(fe1TLX$td, fe2TLX$td, "two.sided") # 
# V = 111, p-value = 0.02795 Z = 2.3205, p-value = 0.02031 d = 0.3979697 
summary(fe1TLX$td) # (MD: 5.667)
sd(fe1TLX$td) # 9.389015
summary(fe2TLX$td) # 4.000
sd(fe2TLX$td) # 4.790803

ttestPairedBatt(fe1TLX$p, fe2TLX$p, "two.sided") # no sig diff.
ttestPairedBatt(fe1TLX$e, fe2TLX$e, "two.sided") # no sig diff.
wilcoxPairedBatt(fe1TLX$Frustration, fe2TLX$Frustration, "two.sided") # no sig diff.
ttestPairedBatt(fe1TLX$o, fe2TLX$o, "two.sided") # no sig diff.
ttestPairedBatt(fe1TLX$o, fe2TLX$o, "greater") # no sig diff.
# --> no significant differences

wilcoxPairedBatt(fe1TLX$o, fe2TLX$o, "less") # significant difference
pearsonRToCohenD(-0.501)

###
# plots

# subject related 
boxplot(fe1TLX$md, fe2TLX$md, fe1TLX$pd, fe2TLX$pd, fe1TLX$td, fe2TLX$td, col=c("limegreen", "lightblue"), main="Weighted NASA TLX Dimensions for\n Demands Imposed on Subject")
axis(side=1, at=c(1:6), labels=c("Mental D. HMD", "Mental D. HMD+P", "Physical D. HMD", "Physical D. HMD+P", "Temporal D. HMD", "Temporal D. HMD+P"))

# task related
boxplot(fe1TLX$p, fe2TLX$p, fe1TLX$e, fe2TLX$e, fe1TLX$td, fe2TLX$td, col=c("limegreen", "lightblue"), main="Weighted NASA TLX Dimensions for\n Interaction with the Task")
axis(side=1, at=c(1:6), labels=c("p HMD", "p HMD+P", "e HMD", "e HMD+P", "Frustration HMD", "Frustration HMD+P"))

# overall
boxplot(fe1TLX$o, fe2TLX$o,  col=c("limegreen", "lightblue"),  ylab="raw score (1-100)")
axis(side=1, at=c(1:2), labels=c("HMD", "HMD + projection"))

summary(fe1TLX$o) 
summary(fe2TLX$o)

# altogether now
boxplot(fe1TLX$md, fe2TLX$md, fe1TLX$pd, fe2TLX$pd, fe1TLX$td, fe2TLX$td, fe1TLX$p, fe2TLX$p, fe1TLX$e, fe2TLX$e, fe1TLX$td, fe2TLX$td, fe1TLX$o, fe2TLX$o, col=c("limegreen", "lightblue"), main="NASA TLX", names=c("MD HMD", "HMD+P", "PD HMD", "HMD+P", "TD HMD", "HMD+P", "P HMD", "HMD+P", "E HMD", "HMD+P", "F HMD", "HMD+P", "Ov HMD", "HMD+P"), ylab="raw score (1-100)")
#axis(side=1, at=c(1:14), labels=c("MD HMD", "HMD+P", "PD HMD", "HMD+P", "TD HMD", "HMD+P", "P HMD", "HMD+P", "E HMD", "HMD+P", "F HMD", "HMD+P", "Ov HMD", "HMD+P"))

# without ov
par("mar" =  c(3, 5, 1, 1))
boxplot(fe1TLX$md, fe2TLX$md, fe1TLX$pd, fe2TLX$pd, fe1TLX$td, fe2TLX$td, fe1TLX$p, fe2TLX$p, fe1TLX$e, fe2TLX$e, fe1TLX$td, fe2TLX$td,  col=c("limegreen", "lightblue"), cex.axis=2,  cex.lab=2)
axis(side=1, at=c(1:12), labels=c("MD HMD", "HMD+P", "PD HMD", "HMD+P", "TD HMD", "HMD+P", "Per HMD", "HMD+P", "Eff HMD", "HMD+P", "Fru HMD", "HMD+P"), cex.axis=2)
#axis(side=1, at=c(1:12), labels=c("MD HMD", "MD HMD+P", "PD HMD", "PD HMD+P", "TD HMD", "TD HMD+P", "Per HMD", "Per HMD+P", "Eff HMD", "Eff HMD+P", "Fru HMD", "Fru HMD+P"), cex.axis=1.8)
#, main="Weighted NASA TLX Dimensions"

###
#functions

# compute wilcoxon signed rank test and report on 
# V, Z, p-value and effect size (pearson r)

wilcoxPairedBatt <- function(x, y, alt)  {
	library(coin)
	
	wt1 <- wilcox.test(x, y, alternative = alt, paired=TRUE)
	print(wt1)
	wt2 <- wilcoxsign_test(x ~ y, exact=TRUE)
	print(wt2)
	# compute pearson r
	zv <- statistic(wt2)
	n <- length(x) + length(y)
	r <- zv/sqrt(n)
	print(r)
}

ttestPairedBatt <- function(x,y, alt) {
	tRes <- t.test(x,y, alternative = alt, paired=TRUE)
	
	# effect ssize (cohen's d)
	# see http://yatani.jp/HCIstats/TTest
	sdDiff <- sd(x -  y)
	meanDiff <- tRes$estimate # str(tres)
	cohensD <- abs(meanDiff) / sdDiff
	
	print(tRes)
	print(cohensD)
}